<?php

namespace App\Http\Controllers\Admin;

use App\Domain\Admin\Guvohnoma\Actions\StoreGuvohnomaAction;
use App\Domain\Admin\Guvohnoma\Actions\UpdateGuvohnomaAction;
use App\Domain\Admin\Guvohnoma\DTO\StoreGuvohnomaDTO;
use App\Domain\Admin\Guvohnoma\DTO\UpdateGuvohnomaDTO;
use App\Domain\Admin\Guvohnoma\Models\Guvohnoma;
use App\Domain\Admin\Guvohnoma\Repositories\GuvohnomaRepository;
use App\Domain\Admin\Guvohnoma\Requests\StoreGuvohnomaRequest;
use App\Domain\Admin\Guvohnoma\Requests\UpdateGuvohnomaRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class GuvohnomaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index(GuvohnomaRepository $guvohnomaRepository)
    {
        $guvohnomas = $guvohnomaRepository->getAll();
        return view('admin.certificate.view', compact('guvohnomas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.certificate.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function store(StoreGuvohnomaRequest $request, StoreGuvohnomaAction $action)
    {
        try {
            $dto = StoreGuvohnomaDTO::fromArray($request->all());
            $action->execute($dto);
        }catch (\Exception $exception)
        {
            return redirect()->back();
        }
        Alert::success('Success Title', 'Гувохнома тури мувофакиятли яратилди.');
        return redirect()->route('guvohnoma.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * @param Guvohnoma $guvohnoma
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|void
     */
    public function edit(Guvohnoma $guvohnoma)
    {
        return view('admin.certificate.edit', compact('guvohnoma'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function update(UpdateGuvohnomaRequest $request, Guvohnoma $guvohnoma, UpdateGuvohnomaAction $action)
    {
        try {
            $request->merge([
                'guvohnoma'=>$guvohnoma
            ]);
            $dto = UpdateGuvohnomaDTO::fromArray($request->all());
            $action->execute($dto);
        }catch (\Exception $exception)
        {
            return redirect()->back();
        }
        Alert::success('Success Title', 'Гувохнома тури мувофакиятли ўзгартирилди.');
        return redirect()->route('guvohnoma.index');
    }

    /**
     * @param Guvohnoma $guvohnoma
     * @return \Illuminate\Http\RedirectResponse|void
     */
    public function destroy(Guvohnoma $guvohnoma)
    {
        $guvohnoma->delete();
        return redirect()->route('guvohnoma.index');
    }
}
