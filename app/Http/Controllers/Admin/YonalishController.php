<?php

namespace App\Http\Controllers\Admin;

use App\Domain\Admin\Directs\Actions\StoreYonalishAction;
use App\Domain\Admin\Directs\Actions\UpdateYonalishAction;
use App\Domain\Admin\Directs\DTO\StoreYonalishDTO;
use App\Domain\Admin\Directs\DTO\UpdateYonalishDTO;
use App\Domain\Admin\Directs\Models\Yonalish;
use App\Domain\Admin\Directs\Requests\StoreYonalishRequest;
use App\Domain\Admin\Directs\Requests\UpdateYonalishRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;
class YonalishController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function index()
    {
        $yonalishs = Yonalish::all();
        return view('admin.study_plan.index', compact('yonalishs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.study_plan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function store(StoreYonalishRequest $request, StoreYonalishAction $action)
    {
        try {
            $dto = StoreYonalishDTO::fromArray($request->all());
            $action->execute($dto);
        }catch (\Exception $exception)
        {
            return redirect()->back();
        }
        Alert::success('Success Title', 'Ўқув режа мувофаққиятли яратилди');
        return redirect()->route('direction.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function edit(Yonalish $direction)
    {
        return view('admin.study_plan.edit', compact('direction'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function update(UpdateYonalishRequest $request, Yonalish $direction, UpdateYonalishAction $action)
    {
        try {
            $request->merge([
                'yonalish'=>$direction
            ]);
            $dto = UpdateYonalishDTO::fromArray($request->all());
            $action->execute($dto);
        }catch (\Exception $exception)
        {
            return redirect()->back();
        }
        Alert::success('Success Title', 'Ўқув режа мувофаққиятли ўзгартирилди');
        return redirect()->route('direction.index');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Yonalish $direction)
    {

        $direction->delete();
        DB::table('courses')->where('stude_plan_id','=', $direction->id)->delete();
        return redirect()->route('direction.index');
    }
}
