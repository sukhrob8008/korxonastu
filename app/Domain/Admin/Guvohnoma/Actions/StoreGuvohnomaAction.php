<?php

namespace App\Domain\Admin\Guvohnoma\Actions;

use App\Domain\Admin\Guvohnoma\DTO\StoreGuvohnomaDTO;
use App\Domain\Admin\Guvohnoma\Models\Guvohnoma;
use Illuminate\Support\Facades\DB;

class StoreGuvohnomaAction
{
    public function execute(StoreGuvohnomaDTO $storeGuvohnomaDTO)
    {
        DB::beginTransaction();
        try {
            $guvohnomas = new Guvohnoma();
            $guvohnomas->tur_title = $storeGuvohnomaDTO->getTurTitle();
            $guvohnomas->save();
        }catch (\Exception $exception)
        {
            DB::rollBack();
            throw $exception;
        }
        DB::commit();
        return $guvohnomas;
    }
}
