<?php

namespace App\Domain\Admin\Students\Models;

use App\Domain\Admin\Groups\Models\Group;
use App\Domain\Admin\Guvohnoma\Models\Guvohnoma;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;

    public function group_name()
    {
        return $this->belongsTo(Group::class, 'group_id', 'id');
    }

    public function type_title()
    {
        return $this->belongsTo(Guvohnoma::class, 'guvohnoma_id','id');
    }

}
