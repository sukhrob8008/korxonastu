<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Image Driver
    |--------------------------------------------------------------------------
    |
    | Intervention Image supports "GD Library" and "Imagick" to process images
    | internally. You may choose one of them according to your PHP
    | configuration. By default PHP's "GD Library" implementation is used.
    |
    | Supported: "gd", "imagick"
    |
    */

    'driver' => 'gd',

    'sizes' => [
        'extra' => [
            'width' => 1024,
            'height' => 720
        ],
        'big' => [
            'width' => 1024,
            'height' => 720
        ],
        'medium' => [
            'width' => 720,
            'height' => 360
        ],
        'small' => [
            'width' => 360,
            'height' => 180
        ],
        'thumb' => [
            'width' => 50,
            'height' => 50
        ],
        'd_small' => [
            'width' => 150,
            'height' => 150
        ],
    ]
];

